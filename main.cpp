#include <QApplication>
#include "controller/login.h"
#include "db/dbconnection.h"
#include "controller/logradouroaddedit.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    LogradouroAddEdit addEdit;
    addEdit.show();
    DbConnection::getDbConnection();


    return a.exec();
}
