#include "dbconnection.h"

QSqlDatabase* DbConnection::dbConnection = nullptr;

DbConnection::DbConnection(QObject *parent) : QObject(parent)
{

}

QSqlDatabase *DbConnection::getDbConnection()
{
    if(dbConnection == nullptr){
        dbConnection = new QSqlDatabase();
        *dbConnection = QSqlDatabase::addDatabase("QPSQL");
        if(!dbConnection->isOpen()){
            dbConnection->setHostName("localhost");
            dbConnection->setDatabaseName("sinanpop86");
            dbConnection->setUserName("default");
            dbConnection->setPassword("secret");
            dbConnection->setConnectOptions("application_name=qtsinan");
            if(dbConnection->open()){
                qDebug() << "Conectado!";
                QSqlQuery query = dbConnection->exec();
                if(query.exec("SET search_path TO qtsinan"))
                {
                    qDebug() << query.executedQuery();
                }else{
                    qDebug() << query.lastError().driverText();
                    qDebug() << query.lastError().databaseText();
                    qDebug() << query.lastError().nativeErrorCode();
                }
            }else {
                qDebug() << dbConnection->lastError().text();
            }
        }
    }
    return dbConnection;
}
