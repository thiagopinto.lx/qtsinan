#ifndef DBCONNECTION_H
#define DBCONNECTION_H

#include <QObject>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QDebug>
#include <QSqlError>

class DbConnection : public QObject, public QSqlDatabase
{
    Q_OBJECT
public:
    explicit DbConnection(QObject *parent = nullptr);
    static QSqlDatabase* getDbConnection();

private:
    static QSqlDatabase* dbConnection;

signals:

public slots:
};

#endif // DBCONNECTION_H
