#-------------------------------------------------
#
# Project created by QtCreator 2019-01-29T20:40:04
#
#-------------------------------------------------

QT       += core gui sql network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QtSinan
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
    controller/login.cpp \
    controller/mainwindow.cpp \
    db/dbconnection.cpp \
    model/users.cpp \
    main.cpp \
    controller/logradouros.cpp \
    controller/logradouroaddedit.cpp \
    controller/bairroaddedit.cpp
HEADERS += \
    controller/login.h \
    controller/mainwindow.h \
    db/dbconnection.h \
    model/users.h \
    controller/logradouros.h \
    controller/logradouroaddedit.h \
    controller/bairroaddedit.h
FORMS += \
    view/login.ui \
    view/mainwindow.ui \
    view/logradouros.ui \
    view/logradouroaddedit.ui \
    view/bairroaddedit.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    db/qtsinan.sql \
    img/qtsinan.png \
    README.md

SUBDIRS +=

RESOURCES += \
    resource/img.qrc
