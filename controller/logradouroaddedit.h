#ifndef LOGRADOUROADDEDIT_H
#define LOGRADOUROADDEDIT_H

#include <QDialog>

namespace Ui {
class LogradouroAddEdit;
}

class LogradouroAddEdit : public QDialog
{
    Q_OBJECT

public:
    explicit LogradouroAddEdit(QWidget *parent = nullptr);
    ~LogradouroAddEdit();

private slots:
    void on_pushButtonCep_clicked();

private:
    Ui::LogradouroAddEdit *ui;
};

#endif // LOGRADOUROADDEDIT_H
