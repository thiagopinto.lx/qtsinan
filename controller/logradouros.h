#ifndef LOGRADOUROS_H
#define LOGRADOUROS_H

#include <QDialog>

namespace Ui {
class Logradouros;
}

class Logradouros : public QDialog
{
    Q_OBJECT

public:
    explicit Logradouros(QWidget *parent = nullptr);
    ~Logradouros();

private:
    Ui::Logradouros *ui;
};

#endif // LOGRADOUROS_H
