#include "controller/logradouroaddedit.h"
#include "ui_logradouroaddedit.h"
#include "controller/bairroaddedit.h"
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QUrl>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QRegExp>
#include <QString>
#include <QMessageBox>

LogradouroAddEdit::LogradouroAddEdit(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LogradouroAddEdit)
{
    ui->setupUi(this);
    ui->comboBoxBairro->addItem("Bairro1", 1);
    ui->comboBoxBairro->addItem("Bairro2", 2);
    ui->comboBoxBairro->addItem("Bairro3", 3);
}

LogradouroAddEdit::~LogradouroAddEdit()
{
    delete ui;
}

void LogradouroAddEdit::on_pushButtonCep_clicked()
{
    QString cep = ui->lineEditCep->text().remove(QRegExp("[^0-9]"));
    qDebug() << cep;



    QNetworkRequest request(QUrl("http://viacep.com.br/ws/"+cep+"/json/unicode/"));
    QNetworkAccessManager accessManager;

    QNetworkReply *reply = accessManager.get(request);

    while (!reply->isFinished()){
        qApp->processEvents(); //verificar isso
    }

    QByteArray response_data = reply->readAll();

    QJsonDocument json = QJsonDocument::fromJson(response_data);
    QJsonObject jsonObject = json.object();
    QString logradouro = jsonObject["logradouro"].toString();
    int bairro = ui->comboBoxBairro->findText(jsonObject["bairro"].toString());
    qDebug() << bairro;
    if(bairro == -1){
        QMessageBox msgBox(this);
        msgBox.setWindowTitle("Deseja incluir Bairro");
        msgBox.setText("Você pode incluir este bairro ou selecionar outro bairro");
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.addButton(tr("Incluir"), QMessageBox::YesRole);
        msgBox.addButton(tr("Selecionar"), QMessageBox::NoRole);

        if(msgBox.exec() == 0){
            Bairroaddedit bairroaddedit(this, jsonObject["bairro"].toString(), jsonObject["uf"].toString(), jsonObject["localidade"].toString());
            bairroaddedit.exec();
        }

        ui->comboBoxBairro->setCurrentIndex(bairro);
    }
    qDebug() << logradouro;
    ui->lineEditLogradouro->setText(logradouro);

}
