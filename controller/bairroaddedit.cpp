#include "controller/bairroaddedit.h"
#include "ui_bairroaddedit.h"

Bairroaddedit::Bairroaddedit(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Bairroaddedit)
{
    ui->setupUi(this);
    QSqlDatabase *db = DbConnection::getDbConnection();
    QSqlQuery query = db->exec();
    if(query.exec("SELECT * FROM qtsinan.estados;"))
    {
        while (query.next()) {
            ui->comboBoxEstado->addItem(query.value("nome").toString(), query.value("sigla").toString());
        }
    }else{
        qDebug() << query.lastError().driverText();
        qDebug() << query.lastError().databaseText();
        qDebug() << query.lastError().nativeErrorCode();
    }
}

Bairroaddedit::Bairroaddedit(QWidget *parent, QString bairro, QString estado, QString cidade) :
    QDialog(parent),
    ui(new Ui::Bairroaddedit)
{
    ui->setupUi(this);
    QSqlDatabase *db = DbConnection::getDbConnection();
    QSqlQuery query = db->exec();
    QString idEstado;
    if(query.exec("SELECT * FROM qtsinan.estados"))
    {
        while (query.next()) {
            ui->comboBoxEstado->addItem(query.value("sigla").toString());
            if(query.value("sigla").toString()==estado){
                idEstado = query.value("id").toString();
            }

        }
    }else{
        qDebug() << query.lastError().driverText();
        qDebug() << query.lastError().databaseText();
        qDebug() << query.lastError().nativeErrorCode();
    }

    if(query.exec("SELECT * FROM qtsinan.cidades WHERE \"idEstado\" = "+ idEstado))
    {
        while (query.next()) {
            ui->comboBoxCidade->addItem(query.value("nome").toString());
        }
    }else{
        qDebug() << query.lastError().driverText();
        qDebug() << query.lastError().databaseText();
        qDebug() << query.lastError().nativeErrorCode();
    }

    if(query.exec("SELECT * FROM qtsinan.distritos"))
    {
        while (query.next()) {
            ui->comboBoxDistrito->addItem(query.value("nome").toString());
        }
    }else{
        qDebug() << query.lastError().driverText();
        qDebug() << query.lastError().databaseText();
        qDebug() << query.lastError().nativeErrorCode();
    }

    ui->lineEditBairro->setText(bairro);
    ui->comboBoxEstado->setCurrentText(estado);
    ui->comboBoxCidade->setCurrentText(cidade.toUpper());
    qDebug() << cidade;
}

Bairroaddedit::~Bairroaddedit()
{
    delete ui;
}
