#ifndef BAIRROADDEDIT_H
#define BAIRROADDEDIT_H

#include <QDialog>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QDebug>
#include <string>
#include "db/dbconnection.h"

namespace Ui {
class Bairroaddedit;
}

class Bairroaddedit : public QDialog
{
    Q_OBJECT

public:
    explicit Bairroaddedit(QWidget *parent = nullptr);
    explicit Bairroaddedit(QWidget *parent = nullptr, QString bairro = "", QString estado = "", QString cidade = "");

    ~Bairroaddedit();

private:
    Ui::Bairroaddedit *ui;
};

#endif // BAIRROADDEDIT_H
